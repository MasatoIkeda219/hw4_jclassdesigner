package jcd.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.DataManager;
import jcd.data.JClass;
import jcd.data.Method;
import jcd.data.Variable;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        StringWriter sw = new StringWriter();
        DataManager dataManager = (DataManager) data;

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        ArrayList<JClass> jClass = dataManager.getJClassArray();
        fillArrayWithJClass(jClass, arrayBuilder);

        JsonArray nodesArray = arrayBuilder.build();
        JsonObject dataManagerJson = Json.createObjectBuilder()
                .add("JClass", nodesArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJson);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJson);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    private void fillArrayWithJClass(ArrayList<JClass> list, JsonArrayBuilder arrayBuilder) {
        for (int i = 0; i < list.size(); i++) {
            JsonObject jObject = makeJClassJsonObject(list.get(i));
            arrayBuilder.add(jObject);
        }
    }

    private JsonObject makeJClassJsonObject(JClass jClass) {

        JsonArrayBuilder varArr = Json.createArrayBuilder();
        JsonArrayBuilder methArr = Json.createArrayBuilder();

        ArrayList<Variable> variables = jClass.getVariables();
        ArrayList<Method> methods = jClass.getMethods();

        fillArrayWithVariable(variables, varArr);
        fillArrayWithMethod(methods, methArr);

        JsonArray varArray = varArr.build();
        JsonArray methArray = methArr.build();

        JsonObject json = Json.createObjectBuilder()
                .add("Class Name", jClass.getName())
                .add("Package Name", jClass.getPackage())
                .add("X", "" + jClass.getVBox().getTranslateX() + "")
                .add("Y", "" + jClass.getVBox().getTranslateY() + "")
                .add("Variable", varArray)
                .add("Method", methArray)
                .add("Parent", jClass.getParent())
                .add("Abstract", jClass.getAbstract())
                .add("Interface", jClass.getInterface())
                .add("API", jClass.getAPI())
                .build();
        return json;
    }

    private void fillArrayWithVariable(ArrayList<Variable> variables, JsonArrayBuilder arrayBuilder) {
        for (int i = 0; i < variables.size(); i++) {
            JsonObject variable = makeVariableJsonObject(variables.get(i));
            arrayBuilder.add(variable);
        }
    }

    private JsonObject makeVariableJsonObject(Variable variable) {
        JsonObject json = Json.createObjectBuilder()
                .add("Name", variable.getName())
                .add("Type", variable.getType())
                .add("Static", variable.getIsStatic())
                .add("Access", variable.getAccess())
                .build();
        return json;
    }

    private void fillArrayWithMethod(ArrayList<Method> methods, JsonArrayBuilder arrayBuilder) {
        for (int i = 0; i < methods.size(); i++) {
            JsonObject variable = makeMethodJsonObject(methods.get(i));
            arrayBuilder.add(variable);
        }
    }

    private JsonObject makeMethodJsonObject(Method method) {

        JsonArrayBuilder argArr = Json.createArrayBuilder();

        ArrayList<String> args = method.getArg();
        fillArrayWithArgs(args, argArr);

        JsonArray argArray = argArr.build();

        JsonObject json = Json.createObjectBuilder()
                .add("Name", method.getName())
                .add("Return", method.getReturnType())
                .add("Static", method.getIsStatic())
                .add("Abstract", method.getIsAbstract())
                .add("Access", method.getAccess())
                .add("Args", argArray)
                .build();
        return json;
    }

    private void fillArrayWithArgs(ArrayList<String> arg, JsonArrayBuilder arrayBuilder) {
        for (int i = 0; i < arg.size(); i++) {
            JsonObject args = Json.createObjectBuilder()
                    .add("Arg", arg.get(i))
                    .build();
            arrayBuilder.add(args);
        }
    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
        dataManager.reset();

        JsonObject json = loadJSONFile(filePath);

        JsonArray jsonJClassArray = json.getJsonArray("JClass");
        loadJClass(jsonJClassArray, dataManager);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    private void loadJClass(JsonArray jsonJClassArray, DataManager dataManager) {
        ArrayList<JClass> list = new ArrayList();
        for (int i = 0; i < jsonJClassArray.size(); i++) {
            JsonObject jsonClass = jsonJClassArray.getJsonObject(i);
            JClass jClass = loadClass(jsonClass);
            list.add(jClass);
        }
        dataManager.setJClassArray(list);
    }

    private JClass loadClass(JsonObject jsonClass) {
        VBox classBox = new VBox();
        VBox nameBox = new VBox();
        VBox varBox = new VBox();
        VBox methBox = new VBox();
        classBox.setMinHeight(150);
        classBox.setMinWidth(250);

        classBox.getChildren().add(nameBox);
        classBox.getChildren().add(varBox);
        classBox.getChildren().add(methBox);

        nameBox.setMinHeight(50);
        nameBox.setMinWidth(250);
        varBox.setMinHeight(50);
        varBox.setMinWidth(250);
        methBox.setMinHeight(50);
        methBox.setMinWidth(250);

        JClass jClass = new JClass(classBox);
        jClass.setName(jsonClass.getString("Class Name"));
        jClass.setPackage(jsonClass.getString("Package Name"));

        JsonArray jsonVar = jsonClass.getJsonArray("Variable");
        ArrayList<Variable> var = loadVariables(jsonVar);
        jClass.setVariable(var);

        JsonArray jsonMeth = jsonClass.getJsonArray("Method");
        ArrayList<Method> meth = loadMethods(jsonMeth);
        jClass.setMethod(meth);

        jClass.setParent(jsonClass.getString("Parent"));

        if (jsonClass.getString("Abstract").equals("true")) {
            jClass.setAbstract(true);
        } else {
            jClass.setAbstract(false);
        }

        if (jsonClass.getString("Interface").equals("true")) {
            jClass.setInterface(true);
        } else {
            jClass.setInterface(false);
        }

        if (jsonClass.getString("API").equals("true")) {
            jClass.setAPI(true);
        } else {
            jClass.setAPI(false);
        }

        return jClass;
    }

    private ArrayList<Variable> loadVariables(JsonArray jsonVar) {
        ArrayList<Variable> var = new ArrayList();
        for (int i = 0; i < jsonVar.size(); i++) {
            JsonObject jsonV = jsonVar.getJsonObject(i);
            Variable variable = loadVariable(jsonV);
            var.add(variable);
        }
        return var;
    }

    private Variable loadVariable(JsonObject var) {
        Variable variable = new Variable();
        variable.setName(var.getString("Name"));
        variable.setType(var.getString("Type"));
        variable.setAccess(var.getString("Access"));
        if (var.getString("Static").equals("true")) {
            variable.setIsStatic(true);
        } else {
            variable.setIsStatic(false);
        }
        return variable;
    }

    private ArrayList<Method> loadMethods(JsonArray jsonMeth) {
        ArrayList<Method> meth = new ArrayList();
        for (int i = 0; i < jsonMeth.size(); i++) {
            JsonObject jsonM = jsonMeth.getJsonObject(i);
            Method method = loadMethod(jsonM);
            meth.add(method);
        }
        return meth;
    }

    private Method loadMethod(JsonObject meth) {
        Method method = new Method();
        method.setName(meth.getString("Name"));
        method.setReturnType(meth.getString("Return"));
        method.setAccess(meth.getString("Access"));
        if (meth.getString("Static").equals("true")) {
            method.setIsStatic(true);
        } else {
            method.setIsStatic(false);
        }
        if (meth.getString("Abstract").equals("true")) {
            method.setIsAbstract(true);
        } else {
            method.setIsAbstract(false);
        }

        JsonArray jsonArg = meth.getJsonArray("Args");
        ArrayList<String> args = loadArgs(jsonArg);
        method.setArg(args);

        return method;
    }

    private ArrayList<String> loadArgs(JsonArray jsonArg) {
        ArrayList<String> args = new ArrayList();
        for (int i = 0; i < jsonArg.size(); i++) {
            JsonObject jsonA = jsonArg.getJsonObject(i);
            String arg = loadArg(jsonA);
            args.add(arg);
        }
        return args;
    }

    private String loadArg(JsonObject arg) {
        String type = arg.getString("Arg");
        return type;
    }

    /**
     * This method exports the contents of the data manager to a Web page
     * including the html page, needed directories, and the CSS file.
     *
     * @param data The data management component.
     *
     * @param filePath Path (including file name/extension) to where to export
     * the page to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

        ArrayList<JClass> list = ((DataManager) data).getJClassArray();
        for (int i = 0; i < list.size(); i++) {
            JClass jClass = list.get(i);
            if (jClass.getAPI().equals("false")) {
                filePath = "work/" + jClass.getPackage();
                File file = new File(filePath);
                file.mkdir();
                file = new File(filePath + "/" + jClass.getName() + ".java");
                file.createNewFile();
                PrintWriter pw = new PrintWriter(file);
                pw.write("public ");
                if (jClass.getAbstract().equals("true")) {
                    pw.write("abstract " + jClass.getName() + "{\n");
                } else if (jClass.getInterface().equals("true")) {
                    pw.write("interface " + jClass.getName() + "{\n");
                } else {
                    pw.write("class " + jClass.getName() + "{\n\n");
                }

                ArrayList<Variable> vars = jClass.getVariables();
                for (int j = 0; j < vars.size(); j++) {
                    Variable variable = vars.get(j);
                    if (!(variable.getAccess().equals("public"))) {
                        pw.write(variable.getAccess() + " ");
                    }
                    if (!(variable.getIsStatic().equals("true"))) {
                        pw.write("static ");
                    }
                    pw.write(variable.getType() + " ");
                    pw.write(variable.getName() + ";\n");
                }
                pw.write("\n");

                ArrayList<Method> meths = jClass.getMethods();
                for (int k = 0; k < meths.size(); k++) {
                    Method meth = meths.get(k);
                    pw.write(meth.getAccess() + " ");
                    if (meth.getIsAbstract().equals("true")) {
                        pw.write("abstract ");
                        pw.write(meth.getReturnType() + " ");
                        pw.write(meth.getName() + ";\n\n");
                    }
                    else{
                        if(meth.getIsStatic().equals("true")){
                            pw.write("static ");
                        }
                        pw.write(meth.getReturnType() + " ");
                        pw.write(meth.getName() + "(){\n}\n\n");
                    }
                }
                pw.write("}");
                pw.close();

            }

        }
    }

    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        // NOTE THAT THE Web Page Maker APPLICATION MAKES
        // NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
        // EXPORTED WEB PAGES
    }
}
