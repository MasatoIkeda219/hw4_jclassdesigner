/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Van
 */
public class JClass {

    String name;
    String pack;
    VBox classBox;
    ArrayList<Variable> variables;
    ArrayList<Method> methods;
    String parentClass;
    boolean isAbstract;
    boolean isInterface;
    boolean isAPI;

    public JClass(VBox box) {
        isAbstract = false;
        isInterface = false;
        classBox = box;
        name = "";
        pack = "";
        variables = new ArrayList();
        methods = new ArrayList();
        parentClass = "";
        isAPI = false;
    }

    public JClass() {
        isAbstract = false;
        isInterface = false;
        classBox = new VBox();
        name = "";
        pack = "";
        variables = new ArrayList();
        methods = new ArrayList();
        parentClass = "";
        isAPI = false;
    }

    public String getName() {
        return name;
    }

    public String getPackage() {
        return pack;
    }

    public ArrayList<Variable> getVariables() {
        return variables;
    }

    public ArrayList<Method> getMethods() {
        return methods;
    }

    public String getParent() {
        return parentClass;
    }

    public String getAbstract() {
        if (isAbstract == true) {
            return "true";
        } else {
            return "false";
        }
    }

    public String getInterface() {
        if (isInterface == true) {
            return "true";
        } else {
            return "false";
        }
    }
    
    public String getAPI() {
        if (isAPI == true) {
            return "true";
        } else {
            return "false";
        }
    }

    public void setName(String nameI) {
        name = nameI;
    }

    public void setPackage(String packI) {
        pack = packI;
    }

    public void setVariable(ArrayList<Variable> variable) {
        variables = variable;
    }

    public void setMethod(ArrayList<Method> method) {
        methods = method;
    }

    public void setParent(String parent) {
        parentClass = parent;
    }

    public void setAbstract(boolean a) {
        isAbstract = a;
    }

    public void setInterface(boolean i) {
        isInterface = i;
    }
    
    public void setAPI(boolean api)
    {
        isAPI = api;
    }

    public VBox getVBox() {
        return classBox;
    }
    
    public void setVBox(VBox box){
        classBox = box;
    }
}
