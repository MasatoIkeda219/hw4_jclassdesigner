/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

/**
 *
 * @author Van
 */
public class Variable {

    String name;
    String type;
    boolean isStatic;
    String access;

    public Variable() {
        name = "";
        type = "";
        isStatic = false;
        access = "";
    }
    

    public void setName(String n) {
        name = n;
    }

    public void setType(String t) {
        type = t;
    }

    public void setIsStatic(boolean s) {
        isStatic = s;
    }

    public void setAccess(String a) {
        access = a;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getIsStatic() {
        if (isStatic == true) {
            return "true";
        } else {
            return "false";
        }
    }

    public String getAccess() {
        return access;
    }

    public String toString() {
        String str = new String();
        if (access.equals("public")) {
            str = "+";
        } else if (access.equals("private")) {
            str = "-";
        } else if (access.equals("protected")) {
            str = "#";
        }

        if (isStatic == true) {
            str = str + "$";
        }

        str = str + name + " : " + type;

        return str;
    }
}
