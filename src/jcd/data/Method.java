/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;

/**
 *
 * @author Van
 */
public class Method {

    String name;
    String returnType;
    boolean isStatic;
    boolean isAbstract;
    String access;
    ArrayList<String> args;

    public Method() {
        name = "";
        returnType = "";
        isStatic = false;
        isAbstract = false;
        access = "";
        args = new ArrayList();
    }

    public void setName(String n) {
        name = n;
    }

    public void setReturnType(String r) {
        returnType = r;
    }

    public void setIsStatic(boolean s) {
        isStatic = s;
    }

    public void setIsAbstract(boolean a) {
        isAbstract = a;
    }

    public void setAccess(String a) {
        access = a;
    }

    public void setArg(ArrayList<String> arg) {
        args = arg;
    }
    
    public void setArgs(String arg){
        args.add(arg);
    }
    

    public String getName() {
        return name;
    }

    public String getReturnType() {
        return returnType;
    }

    public String getIsStatic() {
        if (isStatic == true) {
            return "true";
        } else {
            return "false";
        }
    }

    public String getIsAbstract() {
        if (isAbstract == true) {
            return "true";
        } else {
            return "false";
        }
    }

    public String getAccess() {
        return access;
    }

    public ArrayList<String> getArg() {
        return args;
    }

    public String getArgs(int i){
        return args.get(i);
    }
    
    public String toString() {
        String str = new String();
        if (access.equals("public")) {
            str = "+";
        } else if (access.equals("private")) {
            str = "-";
        } else if (access.equals("protected")) {
            str = "#";
        }

        if (isStatic == true) {
            str = str + "$";
        }

        if (args.isEmpty() == false) {
            str = str + name + "(";
            for (int i = 0; i < args.size(); i++) {
                int count = i + 1;
                str = str + "arg" + count + " : " + getArg().get(i);
                if (count != args.size()) {
                    str = str + ", ";
                }
                else
                {
                    str = str + ")";
                }
            }
        } else {
            str = str + name + "()";
        }

        str = str + " : " + returnType;

        if (isAbstract == true) {
            str = str + " {abstract}";
        }
        return str;
    }
}
