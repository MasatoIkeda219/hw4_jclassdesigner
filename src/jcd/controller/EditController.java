/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jcd.data.DataManager;
import jcd.data.JClass;
import jcd.data.Method;
import jcd.data.Variable;
import jcd.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author Van
 */
public class EditController {

    AppTemplate app;

    DataManager dataManager;

    VBox classBox;
    VBox nameBox;
    VBox varBox;
    VBox methBox;

    Boolean select;
    VBox selectedBox;

    JClass jClass;
    int i;

    TextField nameT;
    TextField packT;

    ArrayList<JClass> list = new ArrayList();
    ArrayList<VBox> listBox = new ArrayList();

    ObservableList<String> parents = FXCollections.observableArrayList();

    public EditController(AppTemplate initApp) {
        app = initApp;
        dataManager = (DataManager) app.getDataComponent();
    }

    public void addClass() {
        Workspace space = (Workspace) app.getWorkspaceComponent();

        nameT = space.getName();
        packT = space.getPackage();

        classBox = new VBox();
        nameBox = new VBox();
        varBox = new VBox();
        methBox = new VBox();

        classBox.setMinHeight(150);
        classBox.setMinWidth(250);

        classBox.getChildren().add(nameBox);
        classBox.getChildren().add(varBox);
        classBox.getChildren().add(methBox);

        nameBox.setMinHeight(50);
        nameBox.setMinWidth(250);
        varBox.setMinHeight(50);
        varBox.setMinWidth(250);
        methBox.setMinHeight(50);
        methBox.setMinWidth(250);

        classBox.getStyleClass().add("jclass_pane");
        nameBox.getStyleClass().add("jclass_pane");
        varBox.getStyleClass().add("jclass_pane");
        methBox.getStyleClass().add("jclass_pane");

        JClass jClass = new JClass(classBox);
        list.add(jClass);
        listBox.add(classBox);
        parents.add(jClass.getName());
        ComboBox par = space.getParents();
        par.setItems(parents);
        nameT.setText("");
        packT.setText("");

        Pane canvas = space.getCanvas();
        canvas.getChildren().add(classBox);
        if (selectedBox != null) {
            selectedBox.setEffect(null);
        }
        selectedBox = classBox;
        selectedBox.setEffect(new DropShadow(100, Color.YELLOW));
    }

    public void addInterface() {
        Workspace space = (Workspace) app.getWorkspaceComponent();

        nameT = space.getName();
        packT = space.getPackage();

        classBox = new VBox();
        nameBox = new VBox();
        varBox = new VBox();
        methBox = new VBox();

        classBox.setMinHeight(150);
        classBox.setMinWidth(250);

        classBox.getChildren().add(nameBox);
        classBox.getChildren().add(varBox);
        classBox.getChildren().add(methBox);

        nameBox.setMinHeight(50);
        nameBox.setMinWidth(250);
        varBox.setMinHeight(50);
        varBox.setMinWidth(250);
        methBox.setMinHeight(50);
        methBox.setMinWidth(250);
        

        classBox.getStyleClass().add("jclass_pane");
        nameBox.getStyleClass().add("jclass_pane");
        varBox.getStyleClass().add("jclass_pane");
        methBox.getStyleClass().add("jclass_pane");

        JClass jClass = new JClass(classBox);
        jClass.setInterface(true);
        list.add(jClass);
        listBox.add(classBox);
        parents.add(jClass.getName());
        ComboBox par = space.getParents();
        par.setItems(parents);

        nameT.setText("");
        packT.setText("");

        Label interf = new Label("<<interface>>");
        nameBox.getChildren().add(interf);

        Pane canvas = space.getCanvas();
        canvas.getChildren().add(classBox);
        if (selectedBox != null) {
            selectedBox.setEffect(null);
        }
        selectedBox = classBox;
        selectedBox.setEffect(new DropShadow(100, Color.YELLOW));
    }

    public void remove() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        int i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        int j = list.indexOf(jClass);
        list.remove(j);
        listBox.remove(i);
        parents.remove(jClass.getName());
        Pane canvas = space.getCanvas();
        canvas.getChildren().remove(selectedBox);
        nameT = space.getName();
        packT = space.getPackage();
        nameT.setText("");
        packT.setText("");
    }

    public void className() {
        Workspace space = (Workspace) app.getWorkspaceComponent();

        nameT = space.getName();
        String name = nameT.getText();
        if (selectedBox != null) {
            int i = listBox.indexOf(selectedBox);
            jClass = list.get(i);
            if (jClass.getInterface().equals("true")) {
                nameBox = (VBox) selectedBox.getChildren().get(0);

                if (nameBox.getChildren().size() == 1) {
                    Label nameL = new Label(name);
                    nameBox.getChildren().add(nameL);

                } else {
                    ((Label) nameBox.getChildren().get(1)).setText(name);
                }

                jClass.setName(name);
                parents.set(i, jClass.getName());
                ComboBox par = space.getParents();
                par.setItems(parents);

            } else {
                nameBox = (VBox) selectedBox.getChildren().get(0);

                if (nameBox.getChildren().isEmpty() == true) {
                    Label nameL = new Label(name);
                    nameBox.getChildren().add(nameL);

                } else {
                    ((Label) nameBox.getChildren().get(0)).setText(name);
                }

                jClass.setName(name);
                parents.set(i, jClass.getName());
                ComboBox par = space.getParents();
                par.setItems(parents);
            }
        }
    }

    public void packageName() {
        Workspace space = (Workspace) app.getWorkspaceComponent();

        packT = space.getPackage();
        String name = packT.getText();
        if (selectedBox != null) {
            int i = listBox.indexOf(selectedBox);
            jClass = list.get(i);
            jClass.setPackage(name);

        }
    }

    public void select(Event e) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        select = space.getSelectState();
        nameT = space.getName();
        packT = space.getPackage();
        ComboBox par = space.getParents();
        
        ObservableList<Variable> dataVar = space.getVarData();
        dataVar.clear();
        ObservableList<Method> dataMeth = space.getMethData();
        dataMeth.clear();

        if (selectedBox != null) {
            selectedBox.setEffect(null);
        }
        if (select == true) {
            if (e.getTarget() instanceof VBox) {
                selectedBox = (VBox) e.getTarget();
                try {
                    selectedBox = (VBox) selectedBox.getParent();
                } catch (Exception f) {
                    //TODO LMAO
                }
                selectedBox.setEffect(new DropShadow(100, Color.YELLOW));
                i = listBox.indexOf(selectedBox);
                jClass = list.get(i);
                nameT.setText(jClass.getName());
                packT.setText(jClass.getPackage());
                ArrayList<Variable> varArr = jClass.getVariables();
                ArrayList<Method> methArr = jClass.getMethods();
                for (int n = 0; n < varArr.size(); n++) {
                    dataVar.add(varArr.get(n));
                }
                for (int j = 0; j < methArr.size(); j++) {
                    dataMeth.add(methArr.get(i));
                }
            } else if (selectedBox != null) {
                selectedBox.setEffect(null);
                selectedBox = null;
                nameT.setText("");
                packT.setText("");
                par.setPlaceholder(null);
            }
        }
    }

    public void move(MouseEvent e) {
        if (selectedBox != null) {
            selectedBox.setLayoutX((e.getX()) - selectedBox.getTranslateX());
            selectedBox.setLayoutY((e.getY()) - selectedBox.getTranslateY());
            selectedBox.setEffect(new DropShadow(100, Color.YELLOW));
            int i = listBox.indexOf(selectedBox);
            JClass jClass = list.get(i);
            jClass.setVBox(selectedBox);
            
        }
    }

    public void addVar() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Variable> dataVar = space.getVarData();
        Variable var = new Variable();
        var.setName("default");
        var.setIsStatic(false);
        var.setAccess("public");
        var.setType("String");
        dataVar.add(var);
        VBox varBox = (VBox) selectedBox.getChildren().get(1);
        Label varLabel = new Label(var.toString());
        varBox.getChildren().add(varLabel);
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        jClass.getVariables().add(var);
    }

    public void removeVar() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Variable> dataVar = space.getVarData();
        TableView<Variable> variableTable = space.getVarTable();
        Variable var = variableTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        dataVar.remove(var);
        i = jClass.getVariables().indexOf(var);
        jClass.getVariables().remove(var);
        VBox varBox = (VBox) selectedBox.getChildren().get(1);
        varBox.getChildren().remove(i);

    }

    public void addMeth() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        Method meth = new Method();
        meth.setName("default");
        meth.setReturnType("int");
        meth.setIsStatic(false);
        meth.setIsAbstract(false);
        meth.setAccess("public");
        dataMeth.add(meth);

        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        Label methLabel = new Label(meth.toString());
        methBox.getChildren().add(methLabel);

        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        jClass.getMethods().add(meth);

    }

    public void removeMeth() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        TableView<Method> methodTable = space.getMethTable();
        Method meth = methodTable.getSelectionModel().getSelectedItem();

        dataMeth.remove(meth);

        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getMethods().indexOf(meth);
        jClass.getMethods().remove(meth);
        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        methBox.getChildren().remove(i);
    }

    public void updateVarName(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Variable> dataVar = space.getVarData();
        TableView<Variable> variableTable = space.getVarTable();
        Variable var = variableTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getVariables().indexOf(var);
        jClass.getVariables().get(i).setName(kek);
        dataVar.get(i).setName(kek);
        VBox varBox = (VBox) selectedBox.getChildren().get(1);
        ((Label) varBox.getChildren().get(i)).setText(jClass.getVariables().get(i).toString());

    }

    public void updateVarType(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Variable> dataVar = space.getVarData();
        TableView<Variable> variableTable = space.getVarTable();
        Variable var = variableTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getVariables().indexOf(var);
        jClass.getVariables().get(i).setType(kek);
        dataVar.get(i).setType(kek);
        VBox varBox = (VBox) selectedBox.getChildren().get(1);
        ((Label) varBox.getChildren().get(i)).setText(jClass.getVariables().get(i).toString());
    }

    public void updateVarStatic(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Variable> dataVar = space.getVarData();
        TableView<Variable> variableTable = space.getVarTable();
        Variable var = variableTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getVariables().indexOf(var);
        if (kek.equals("true")) {
            jClass.getVariables().get(i).setIsStatic(true);
            dataVar.get(i).setIsStatic(true);
        } else {
            jClass.getVariables().get(i).setIsStatic(false);
            dataVar.get(i).setIsStatic(false);
        }
        VBox varBox = (VBox) selectedBox.getChildren().get(1);
        ((Label) varBox.getChildren().get(i)).setText(jClass.getVariables().get(i).toString());
    }

    public void updateVarAccess(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Variable> dataVar = space.getVarData();
        TableView<Variable> variableTable = space.getVarTable();
        Variable var = variableTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getVariables().indexOf(var);
        jClass.getVariables().get(i).setAccess(kek);
        dataVar.get(i).setAccess(kek);
        VBox varBox = (VBox) selectedBox.getChildren().get(1);
        ((Label) varBox.getChildren().get(i)).setText(jClass.getVariables().get(i).toString());
    }

    public void updateMethName(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        TableView<Method> methodTable = space.getMethTable();
        Method meth = methodTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getMethods().indexOf(meth);
        jClass.getMethods().get(i).setName(kek);
        dataMeth.get(i).setName(kek);
        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        ((Label) methBox.getChildren().get(i)).setText(jClass.getMethods().get(i).toString());
    }

    public void updateMethStatic(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        TableView<Method> methodTable = space.getMethTable();
        Method meth = methodTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getMethods().indexOf(meth);
        if (kek.equals("true")) {
            jClass.getMethods().get(i).setIsStatic(true);
            dataMeth.get(i).setIsStatic(true);
        } else {
            jClass.getMethods().get(i).setIsStatic(false);
            dataMeth.get(i).setIsStatic(false);
        }
        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        ((Label) methBox.getChildren().get(i)).setText(jClass.getMethods().get(i).toString());
    }

    public void updateMethReturn(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        TableView<Method> methodTable = space.getMethTable();
        Method meth = methodTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getMethods().indexOf(meth);
        jClass.getMethods().get(i).setReturnType(kek);
        dataMeth.get(i).setReturnType(kek);
        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        ((Label) methBox.getChildren().get(i)).setText(jClass.getMethods().get(i).toString());
    }

    public void updateMethAccess(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        TableView<Method> methodTable = space.getMethTable();
        Method meth = methodTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getMethods().indexOf(meth);
        jClass.getMethods().get(i).setAccess(kek);
        dataMeth.get(i).setAccess(kek);
        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        ((Label) methBox.getChildren().get(i)).setText(jClass.getMethods().get(i).toString());
    }

    public void updateMethAbstract(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        TableView<Method> methodTable = space.getMethTable();
        Method meth = methodTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getMethods().indexOf(meth);
        if (kek.equals("true")) {
            jClass.getMethods().get(i).setIsAbstract(true);
            dataMeth.get(i).setIsAbstract(true);
            if (jClass.getInterface().equals("false")) {
                if (jClass.getAbstract().equals("false")) {
                    jClass.setAbstract(true);
                    VBox nameBox = (VBox) selectedBox.getChildren().get(0);
                    Label abstractLabel = new Label("{abstract}");
                    nameBox.getChildren().add(abstractLabel);
                }
            }
        } else {
            jClass.getMethods().get(i).setIsAbstract(false);
            dataMeth.get(i).setIsAbstract(false);
        }
        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        ((Label) methBox.getChildren().get(i)).setText(jClass.getMethods().get(i).toString());

    }

    public void updateMethArg(String kek) {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ObservableList<Method> dataMeth = space.getMethData();
        TableView<Method> methodTable = space.getMethTable();
        Method meth = methodTable.getSelectionModel().getSelectedItem();
        i = listBox.indexOf(selectedBox);
        jClass = list.get(i);
        i = jClass.getMethods().indexOf(meth);
        jClass.getMethods().get(i).setArgs(kek);
        //dataMeth.get(i).setArgs(kek);
        VBox methBox = (VBox) selectedBox.getChildren().get(2);
        ((Label) methBox.getChildren().get(i)).setText(jClass.getMethods().get(i).toString());

    }

    public void snapshot() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        Pane canvas = space.getCanvas();
        FileChooser save = new FileChooser();
        save.setTitle("Save Snapshot As...");
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("PNG files *.png", "*.png");
        save.getExtensionFilters().add(filter);
        File snapFile = save.showSaveDialog(app.getGUI().getWindow());
        WritableImage snap = canvas.snapshot(new SnapshotParameters(), null);
        if (snapFile != null) {
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(snap, null), "png", snapFile);
            } catch (IOException h) {
                System.out.println("invalid");
            }
        }
    }

    public void editParent() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        ComboBox par = space.getParents();
        par.setItems(parents);
        i = listBox.indexOf(selectedBox);
        JClass jclass = list.get(i);
        jClass.setParent((String) par.getValue());
                
       // VBox parentBox = (VBox)listBox.
       // drawLine(selectedBox.getTranslateX(), selectedBox.getTranslateY(), );
    }
    
    public void resize(MouseEvent e){
        if (selectedBox != null) {
            selectedBox.setMinWidth((e.getX()) - selectedBox.getTranslateX());
            selectedBox.setMinHeight((e.getY()) - selectedBox.getTranslateY());
            
            VBox nameBox = (VBox)selectedBox.getChildren().get(0);
            nameBox.setMinHeight((e.getY()) - selectedBox.getTranslateY()/3);
            VBox varBox = (VBox)selectedBox.getChildren().get(1);
            varBox.setMinHeight((e.getY()) - selectedBox.getTranslateY()/3);
            VBox methBox = (VBox)selectedBox.getChildren().get(2);
            methBox.setMinHeight((e.getY()) - selectedBox.getTranslateY()/3);
             
            
        }
    }

    public ArrayList<JClass> getList() {
        return list;
    }

    public void setBox(ArrayList<VBox> listB) {
        listBox = listB;
    }

    public void setList(ArrayList<JClass> listJ) {
        list = listJ;
    }
    
    public void drawLine(Double startX, Double startY, Double endX, Double endY){
        Line line = new Line();
        line.setStartX(startX);
        line.setStartY(startY);
        line.setEndX(endX);
        line.setEndY(endY);
    }
}
