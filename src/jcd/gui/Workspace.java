package jcd.gui;

import java.io.File;
import javafx.scene.control.Button;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import saf.components.AppFileComponent;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import static jcd.PropertyType.ADD_CLASS_ICON;
import static jcd.PropertyType.ADD_CLASS_TOOLTIP;
import static jcd.PropertyType.ADD_ICON;
import static jcd.PropertyType.ADD_INTERFACE_ICON;
import static jcd.PropertyType.ADD_INTERFACE_TOOLTIP;
import static jcd.PropertyType.REDO_ICON;
import static jcd.PropertyType.REDO_TOOLTIP;
import static jcd.PropertyType.REMOVE_TOOLTIP;
import static jcd.PropertyType.REMOVE_ICON;
import static jcd.PropertyType.REM_ICON;
import static jcd.PropertyType.RESIZE_ICON;
import static jcd.PropertyType.RESIZE_TOOLTIP;
import static jcd.PropertyType.SELECTION_TOOL_ICON;
import static jcd.PropertyType.SELECTION_TOOL_TOOLTIP;
import static jcd.PropertyType.UNDO_ICON;
import static jcd.PropertyType.UNDO_TOOLTIP;
import static jcd.PropertyType.ZOOM_IN_ICON;
import static jcd.PropertyType.ZOOM_IN_TOOLTIP;
import static jcd.PropertyType.ZOOM_OUT_ICON;
import static jcd.PropertyType.ZOOM_OUT_TOOLTIP;
import jcd.controller.ComponentEditController;
import jcd.controller.EditController;
import jcd.controller.ViewEditController;
import jcd.data.DataManager;
import jcd.data.JClass;
import jcd.data.Method;
import jcd.data.Variable;
import jcd.file.FileManager;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import saf.controller.AppFileController;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    HBox topBar = new HBox();
    HBox editBar = new HBox();
    HBox viewBar = new HBox();
    VBox checks = new VBox();
    VBox compToolbar = new VBox();

    ComboBox parentNameText = new ComboBox();
    boolean resize = false;

    Button selectButton;
    Button resizeButton;
    Button addClassButton;
    Button addInterfaceButton;
    Button removeButton;
    Button undoButton;
    Button redoButton;

    Button zoomInButton;
    Button zoomOutButton;
    CheckBox gridButton;
    CheckBox snapButton;

    Button addVariable;
    Button removeVariable;

    Button addMethod;
    Button removeMethod;

    Button addArgs;

    TextField classNameText;
    TextField packageNameText;

    Boolean select = false;
    VBox selectedBox;

    ScrollPane scroll;
    HBox classPane = new HBox();
    HBox packagePane = new HBox();
    HBox parentPane = new HBox();
    HBox variablePane = new HBox();
    HBox methodPane = new HBox();
    Pane canvas = new Pane();
    HBox argPane = new HBox();

    ComponentEditController cec;
    EditController ec;
    ViewEditController vec;

    ObservableList<Variable> dataVar = FXCollections.observableArrayList();
    TableView<Variable> variableTable = new TableView<>();

    ObservableList<Method> dataMeth = FXCollections.observableArrayList();
    TableView<Method> methodTable = new TableView();

    AppFileController ac = new AppFileController(app);

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        workspace = new BorderPane();

        ec = new EditController(app);
        DataManager dataManager = (DataManager) app.getDataComponent();
        dataManager.setJClassArray(ec.getList());

        topBar = gui.getFileToolBar();

        editBar.getStyleClass().add("edit_pane");
        canvas.getStyleClass().add("canvas_pane");
        compToolbar.getStyleClass().add("component_pane");

        topBar.getChildren().add(editBar);
        selectButton = initChildButton(editBar, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), false);
        resizeButton = initChildButton(editBar, RESIZE_ICON.toString(), RESIZE_TOOLTIP.toString(), false);
        addClassButton = initChildButton(editBar, ADD_CLASS_ICON.toString(), ADD_CLASS_TOOLTIP.toString(), false);
        addInterfaceButton = initChildButton(editBar, ADD_INTERFACE_ICON.toString(), ADD_INTERFACE_TOOLTIP.toString(), false);
        removeButton = initChildButton(editBar, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), false);
        undoButton = initChildButton(editBar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), true);
        redoButton = initChildButton(editBar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), true);

        viewBar.getStyleClass().add("edit_pane");

        topBar.getChildren().add(viewBar);
        zoomInButton = initChildButton(viewBar, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), false);
        zoomOutButton = initChildButton(viewBar, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), false);
        gridButton = new CheckBox("Grid");
        snapButton = new CheckBox("Snap");

        viewBar.getChildren().add(checks);
        checks.getChildren().add(gridButton);
        checks.getChildren().add(snapButton);

        canvas.setPrefSize(1500, 1500);

        scroll = new ScrollPane(canvas);

        Label classNameLabel = new Label("Class Name:   ");
        classNameText = new TextField();
        classNameLabel.getStyleClass().add("font_big");
        classPane.getChildren().add(classNameLabel);
        classPane.getChildren().add(classNameText);
        classPane.setAlignment(Pos.CENTER_LEFT);
        compToolbar.getChildren().add(classPane);

        Label packageNameLabel = new Label("Package :             ");
        packageNameText = new TextField();
        packageNameLabel.getStyleClass().add("font_med");
        packagePane.getChildren().add(packageNameLabel);
        packagePane.getChildren().add(packageNameText);
        packagePane.setAlignment(Pos.CENTER_LEFT);
        compToolbar.getChildren().add(packagePane);

        Label parentNameLabel = new Label("Parent :                ");
        parentNameText.setMinWidth(150);
        parentNameLabel.getStyleClass().add("font_med");
        parentPane.getChildren().add(parentNameLabel);
        parentPane.getChildren().add(parentNameText);
        parentPane.setAlignment(Pos.CENTER_LEFT);
        compToolbar.getChildren().add(parentPane);

        Label variableLabel = new Label("Variable :      ");
        variableLabel.getStyleClass().add("font_med");
        variablePane.getChildren().add(variableLabel);
        addVariable = initChildButton(variablePane, ADD_ICON.toString(), "", false);
        removeVariable = initChildButton(variablePane, REM_ICON.toString(), "", false);
        variablePane.setAlignment(Pos.CENTER_LEFT);
        compToolbar.getChildren().add(variablePane);

        ec = new EditController(app);

        ObservableList<String> accessBox
                = FXCollections.observableArrayList("public", "private", "protected");
        ObservableList<String> staticBox
                = FXCollections.observableArrayList("true", "false");
        ObservableList<String> abstractBox
                = FXCollections.observableArrayList("true", "false");
        variableTable.setEditable(true);
        TableColumn<Variable, String> nameC = new TableColumn("▼ Name");
        nameC.setCellValueFactory(
                new PropertyValueFactory<>("name"));
        nameC.setCellFactory(TextFieldTableCell.forTableColumn());
        nameC.setOnEditCommit(e -> {
            ec.updateVarName(e.getNewValue());
        });
        TableColumn<Variable, String> typeC = new TableColumn("▼ Type");
        typeC.setCellValueFactory(
                new PropertyValueFactory<>("type"));
        typeC.setCellFactory(TextFieldTableCell.forTableColumn());
        typeC.setOnEditCommit(e -> {
            ec.updateVarType(e.getNewValue());
        });
        TableColumn<Variable, String> staticC = new TableColumn("▼ Static");
        staticC.setCellValueFactory(
                new PropertyValueFactory<>("isStatic"));
        staticC.setCellFactory(ComboBoxTableCell.forTableColumn(staticBox));
        staticC.setOnEditCommit(e -> {
            ec.updateVarStatic(e.getNewValue());
        });
        TableColumn<Variable, String> accessC = new TableColumn("▼ Access");
        accessC.setCellValueFactory(
                new PropertyValueFactory<>("access"));
        accessC.setCellFactory(ComboBoxTableCell.forTableColumn(accessBox));
        accessC.setOnEditCommit(e -> {
            ec.updateVarAccess(e.getNewValue());
        });
        variableTable.getColumns().addAll(nameC, typeC, staticC, accessC);
        variableTable.setItems(dataVar);
        ScrollPane variableScroll = new ScrollPane(variableTable);

        compToolbar.getChildren().add(variableScroll);

        Label methodLabel = new Label("Method :      ");
        methodLabel.getStyleClass().add("font_med");
        methodPane.getChildren().add(methodLabel);
        addMethod = initChildButton(methodPane, ADD_ICON.toString(), "", false);
        removeMethod = initChildButton(methodPane, REM_ICON.toString(), "", false);
        methodPane.setAlignment(Pos.CENTER_LEFT);
        compToolbar.getChildren().add(methodPane);

        methodTable.setEditable(true);
        TableColumn<Method, String> nameCM = new TableColumn("▼ Name");
        nameCM.setCellValueFactory(
                new PropertyValueFactory<>("name"));
        nameCM.setCellFactory(TextFieldTableCell.forTableColumn());
        nameCM.setOnEditCommit(e -> {
            ec.updateMethName(e.getNewValue());
        });
        TableColumn<Method, String> staticCM = new TableColumn("▼ Static");
        staticCM.setCellValueFactory(
                new PropertyValueFactory<>("isStatic"));
        staticCM.setCellFactory(ComboBoxTableCell.forTableColumn(staticBox));
        staticCM.setOnEditCommit(e -> {
            ec.updateMethStatic(e.getNewValue());
        });
        TableColumn<Method, String> accessCM = new TableColumn("▼ Access");
        accessCM.setCellValueFactory(
                new PropertyValueFactory<>("access"));
        accessCM.setCellFactory(ComboBoxTableCell.forTableColumn(accessBox));
        accessCM.setOnEditCommit(e -> {
            ec.updateMethAccess(e.getNewValue());
        });
        TableColumn<Method, String> returnC = new TableColumn("▼ Return");
        returnC.setCellFactory(TextFieldTableCell.forTableColumn());
        returnC.setCellValueFactory(
                new PropertyValueFactory<>("returnType"));
        returnC.setOnEditCommit(e -> {
            ec.updateMethReturn(e.getNewValue());
        });
        TableColumn<Method, String> abstractC = new TableColumn("▼ Abstract");
        abstractC.setCellFactory(ComboBoxTableCell.forTableColumn(abstractBox));
        abstractC.setCellValueFactory(
                new PropertyValueFactory<>("isAbstract"));
        abstractC.setOnEditCommit(e -> {
            ec.updateMethAbstract(e.getNewValue());
        });
        TableColumn<Method, String> argC = new TableColumn("▼ Arg");
        argC.setCellFactory(TextFieldTableCell.forTableColumn());
        argC.setCellValueFactory(
                new PropertyValueFactory<>("args"));
        argC.setOnEditCommit(e -> {
            ec.updateMethArg(e.getNewValue());
        });
        methodTable.setItems(dataMeth);

        methodTable.getColumns().addAll(nameCM, returnC, staticCM, abstractC, accessCM, argC);
        ScrollPane methodScroll = new ScrollPane(methodTable);
        compToolbar.getChildren().add(methodScroll);

        compToolbar.setMaxWidth(500);
        ((BorderPane) workspace).setCenter(scroll);
        ((BorderPane) workspace).setRight(compToolbar);

        setupHandlers();

    }

    public Button initChildButton(Pane toolbar, String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);

        // NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);

        // PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);

        // AND RETURN THE COMPLETED BUTTON
        return button;
    }

    private void setupHandlers() {
        ec = new EditController(app);

        addClassButton.setOnAction(e -> {
            ac.markAsEdited(gui);
            select = true;
            ec.addClass();
            resize = false;
        });

        addInterfaceButton.setOnAction(e -> {
            ac.markAsEdited(gui);
            select = true;
            ec.addInterface();
            resize = false;
        });

        removeButton.setOnAction(e -> {
            ac.markAsEdited(gui);
            ec.remove();
            resize = false;
        });

        selectButton.setOnAction(e -> {
            select = true;
            ec.select(e);
            resize = false;
        });

        classNameText.setOnKeyReleased(e -> {
            ac.markAsEdited(gui);
            ec.className();
            resize = false;
        });

        packageNameText.setOnKeyReleased(e -> {
            ac.markAsEdited(gui);
            ec.packageName();
            resize = false;
        });

        canvas.setOnMouseClicked(e -> {
            if (select == true) {
                ec.select(e);
            }
        });

        canvas.setOnMouseDragged(e -> {
            ac.markAsEdited(gui);
            if (select == true) {
                ec.move(e);
            }
            if (resize == true) {
                ec.resize(e);
            }
        });

        addVariable.setOnAction(e -> {
            ac.markAsEdited(gui);
            ec.addVar();
            resize = false;
        });

        removeVariable.setOnAction(e -> {
            ac.markAsEdited(gui);
            ec.removeVar();
            resize = false;
        });

        addMethod.setOnAction(e -> {
            ac.markAsEdited(gui);
            ec.addMeth();
            resize = false;
        });

        removeMethod.setOnAction(e -> {
            ac.markAsEdited(gui);
            ec.removeMeth();
            resize = false;
        });

        Button codeButton = gui.getExport();
        FileManager fm = new FileManager();
        DataManager dataManager = (DataManager) app.getDataComponent();
        codeButton.setOnAction(e -> {
            resize = false;
            try {
                fm.exportData(dataManager, "work/");
            } catch (IOException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        Button photoButton = gui.getPhoto();
        photoButton.setOnAction(e -> {
            ec.snapshot();
            resize = false;
        });

        parentNameText.setOnAction(e -> {
            ec.editParent();
            resize = false;
        });

        resizeButton.setOnAction(e -> {
            resize = true;
            select = false;
        });

    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {

    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        canvas.getChildren().clear();
        ArrayList<JClass> list = dataManager.getJClassArray();
        ArrayList<VBox> listBox = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            VBox classBox = list.get(i).getVBox();

            classBox.getStyleClass().add("jclass_pane");
            classBox.getChildren().get(0).getStyleClass().add("jclass_pane");
            classBox.getChildren().get(1).getStyleClass().add("jclass_pane");
            classBox.getChildren().get(2).getStyleClass().add("jclass_pane");

            Label name = new Label(list.get(i).getName());
            VBox nameBox = (VBox) classBox.getChildren().get(0);
            nameBox.getChildren().add(name);

            VBox varBox = (VBox) classBox.getChildren().get(1);
            for (int j = 0; j < list.get(i).getVariables().size(); j++) {
                Label var = new Label(list.get(i).getVariables().get(j).toString());
                varBox.getChildren().add(var);
            }

            VBox methBox = (VBox) classBox.getChildren().get(2);
            for (int k = 0; k < list.get(i).getMethods().size(); k++) {
                Label meth = new Label(list.get(i).getMethods().get(k).toString());
                methBox.getChildren().add(meth);
            }

            if (list.get(i).getAbstract().equals("true")) {
                Label abstractLabel = new Label("{abstract}");
                nameBox.getChildren().add(abstractLabel);
            }

            if (list.get(i).getInterface().equals("true")) {
                Label interfaceLabel = new Label("<<interface>>");
                nameBox.getChildren().add(interfaceLabel);
            }

            canvas.getChildren().add(classBox);
            listBox.add(classBox);

        }
        ec = new EditController(app);
        ec.setBox(listBox);
        ec.setList(list);
    }

    public Pane getCanvas() {
        return canvas;
    }

    public TextField getName() {
        return classNameText;
    }

    public boolean getSelectState() {
        return select;
    }

    public TextField getPackage() {
        return packageNameText;
    }

    public ObservableList<Variable> getVarData() {
        return dataVar;
    }

    public ObservableList<Method> getMethData() {
        return dataMeth;
    }

    public TableView<Variable> getVarTable() {
        return variableTable;
    }

    public TableView<Method> getMethTable() {
        return methodTable;
    }

    public ComboBox getParents() {
        return parentNameText;
    }

}
