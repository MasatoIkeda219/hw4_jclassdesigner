/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import jcd.data.DataManager;
import jcd.data.JClass;
import jcd.file.FileManager;

/**
 *
 * @author Van
 */
public class TestLoad {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        FileManager fm = new FileManager();
        DataManager data = new DataManager();
        
        String filePath = "work/DesignSaveTest.json";
        
        fm.loadData(data, filePath);
        
        ArrayList<JClass> list = data.getJClassArray();
        
        System.out.println(list.get(0).getName());
        System.out.println(list.get(0).getPackage());
        System.out.println(list.get(0).getMethods().get(5).getName());
        System.out.println(list.get(0).getVariables().get(3).getName());
        System.out.println(list.get(0).getMethods().get(0).getArg().get(0));
        
        
        
    }
    
}
