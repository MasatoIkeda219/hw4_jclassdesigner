 /* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import jcd.data.DataManager;
import jcd.data.JClass;
import jcd.data.Method;
import jcd.data.Variable;
import jcd.file.FileManager;

/**
 *
 * @author Van
 */
public class TestSave {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        ArrayList<JClass> list = new ArrayList();
        
        JClass app = new JClass();
        app.setName("Application");
        app.setPackage("javafx.application");
        app.setAbstract(true);
        app.setInterface(false);
        app.setAPI(true);
        
        ArrayList<Method> appM = new ArrayList();
        Method startApp = new Method();
        startApp.setName("start");
        startApp.setAccess("public");
        startApp.setIsAbstract(true);
        startApp.setReturnType("void");
        startApp.setIsStatic(false);
        ArrayList<String> StartAA = new ArrayList();
        String ar = "Stage";
        StartAA.add(ar);
        startApp.setArg(StartAA);
        appM.add(startApp);
        app.setMethod(appM);
        
        list.add(app);
        
        JClass te = new JClass();
        te.setName("ThreadExample");
        te.setPackage("default package");
        ArrayList<Variable> teV = new ArrayList();
        
        Variable st = new Variable();
        st.setName("START_TEXT");
        st.setType("String");
        st.setAccess("public");
        st.setIsStatic(true);
        teV.add(st);
        
        Variable pt = new Variable();
        pt.setName("PAUSE_TEXT");
        pt.setType("String");
        pt.setAccess("public");
        pt.setIsStatic(true);
        teV.add(pt);
        
        Variable win = new Variable();
        win.setName("window");
        win.setType("Stage");
        win.setAccess("private");
        win.setIsStatic(false);
        teV.add(win);
        
        Variable ap = new Variable();
        ap.setName("appPane");
        ap.setType("BorderPane");
        ap.setAccess("private");
        ap.setIsStatic(false);
        teV.add(ap);
        
        Variable tp = new Variable();
        tp.setName("topPane");
        tp.setType("FlowPane");
        tp.setAccess("private");
        tp.setIsStatic(false);
        teV.add(tp);
        
        Variable sb = new Variable();
        sb.setName("startButton");
        sb.setType("Button");
        sb.setAccess("private");
        sb.setIsStatic(false);
        teV.add(sb);
        
        Variable pb = new Variable();
        pb.setName("pauseButton");
        pb.setType("Button");
        pb.setAccess("private");
        pb.setIsStatic(false);
        teV.add(pb);
        
        Variable sp = new Variable();
        sp.setName("scrollPane");
        sp.setType("ScrollPane");
        sp.setAccess("private");
        sp.setIsStatic(false);
        teV.add(sp);
        
        Variable ta = new Variable();
        ta.setName("textArea");
        ta.setType("TextArea");
        ta.setAccess("private");
        ta.setIsStatic(false);
        teV.add(ta);
        
        Variable dTh = new Variable();
        dTh.setName("dateThread");
        dTh.setType("Thread");
        dTh.setAccess("private");
        dTh.setIsStatic(false);
        teV.add(dTh);
        
        Variable dt = new Variable();
        dt.setName("dateTask");
        dt.setType("Task");
        dt.setAccess("private");
        dt.setIsStatic(false);
        teV.add(dt);
        
        Variable cTh = new Variable();
        cTh.setName("counterThread");
        cTh.setType("Thread");
        cTh.setAccess("private");
        cTh.setIsStatic(false);
        teV.add(cTh);
        
        Variable cT = new Variable();
        cT.setName("counterTask");
        cT.setType("Task");
        cT.setAccess("private");
        cT.setIsStatic(false);
        teV.add(cT);
        
        Variable wk = new Variable();
        wk.setName("window");
        wk.setType("Stage");
        wk.setAccess("private");
        wk.setIsStatic(false);
        teV.add(wk);
        
        te.setVariable(teV);
        
        ArrayList<Method> teM = new ArrayList();
        
        Method start = new Method();
        start.setName("start");
        start.setIsAbstract(false);
        start.setAccess("public");
        start.setIsStatic(false);
        start.setReturnType("void");
        ArrayList<String> startA = new ArrayList();
        String arg = "Stage";
        startA.add(arg);
        start.setArg(startA);
        teM.add(start);
        
        Method sw = new Method();
        sw.setName("startWork");
        sw.setIsAbstract(false);
        sw.setAccess("public");
        sw.setIsStatic(false);
        sw.setReturnType("void");
        teM.add(sw);
        
        Method pw = new Method();
        pw.setName("pauseWork");
        pw.setIsAbstract(false);
        pw.setAccess("public");
        pw.setIsStatic(false);
        pw.setReturnType("void");
        teM.add(pw);
        
        Method dw = new Method();
        dw.setName("doWork");
        dw.setIsAbstract(false);
        dw.setAccess("public");
        dw.setIsStatic(false);
        start.setReturnType("boolean");
        teM.add(dw);
        
        Method at = new Method();
        at.setName("appendText");
        at.setIsAbstract(false);
        at.setAccess("public");
        at.setIsStatic(false);
        at.setReturnType("void");
        ArrayList<String> atA = new ArrayList();
        arg = "String";
        atA.add(arg);
        at.setArg(atA);
        teM.add(at);
        
        Method sl = new Method();
        sl.setName("sleep");
        sl.setIsAbstract(false);
        sl.setAccess("public");
        sl.setIsStatic(false);
        sl.setReturnType("void");
        ArrayList<String> slA = new ArrayList();
        arg = "int";
        slA.add(arg);
        sl.setArg(slA);
        teM.add(sl);
        
        Method il = new Method();
        il.setName("initLayout");
        il.setIsAbstract(false);
        il.setAccess("private");
        il.setIsStatic(false);
        il.setReturnType("void");
        teM.add(il);
        
        Method ih = new Method();
        ih.setName("initHandlers");
        ih.setIsAbstract(false);
        ih.setAccess("private");
        ih.setIsStatic(false);
        ih.setReturnType("void");
        teM.add(ih);
        
        Method iw = new Method();
        iw.setName("initWindow");
        iw.setIsAbstract(false);
        iw.setAccess("private");
        iw.setIsStatic(false);
        iw.setReturnType("void");
        ArrayList<String> iwA = new ArrayList();
        arg = "Stage";
        iwA.add(arg);
        start.setArg(iwA);
        teM.add(iw);
        
        Method it = new Method();
        it.setName("initThrreads");
        it.setIsAbstract(false);
        it.setAccess("private");
        it.setIsStatic(false);
        it.setReturnType("void");
        teM.add(it);
        
        Method main = new Method();
        main.setName("main");
        main.setIsAbstract(false);
        main.setAccess("public");
        main.setIsStatic(true);
        main.setReturnType("void");
        ArrayList<String> mainA = new ArrayList();
        arg = "String[]";
        mainA.add(arg);
        main.setArg(mainA);
        teM.add(main);
        
        te.setMethod(teM);
        
        te.setAbstract(false);
        te.setInterface(false);
        te.setParent("Application");
        te.setAPI(false);

        
        list.add(te);
        
        JClass ct = new JClass();
        ct.setName("CounterTask");
        ct.setPackage("default package");
        ct.setAbstract(false);
        ct.setInterface(false);
        ct.setParent("Task");
        ct.setAPI(false);
        
        ArrayList<Variable> ctV = new ArrayList();
        Variable appCT = new Variable();
        appCT.setName("app");
        appCT.setAccess("private");
        appCT.setIsStatic(false);
        appCT.setType("ThreadExample");
        ctV.add(appCT);
        Variable count= new Variable();
        count.setAccess("private");
        count.setName("counter");
        count.setIsStatic(false);
        count.setType("int");
        ctV.add(count);
        ct.setVariable(ctV);
        
        ArrayList<Method> ctM = new ArrayList();
        Method ctm = new Method();
        ctm.setIsAbstract(false);
        ctm.setAccess("public");
        ctm.setName("CounterTask");
        ctm.setIsStatic(false);
        ArrayList ctmA = new ArrayList();
        arg = "ThreadExample";
        ctmA.add(arg);
        ctm.setArg(ctmA);
        ctM.add(ctm);
        
        Method callCT = new Method();
        callCT.setIsAbstract(false);
        callCT.setAccess("protected");
        callCT.setName("call");
        callCT.setReturnType("void");
        callCT.setIsStatic(false);
        ctM.add(callCT);

        ct.setMethod(ctM);
        
        list.add(ct);
        
        JClass dtC = new JClass();
        dtC.setName("DateTask");
        dtC.setPackage("default package");
        dtC.setAbstract(false);
        dtC.setInterface(false);
        dtC.setParent("Task");
        dtC.setAPI(false);
        
        ArrayList<Variable> dtV = new ArrayList();
        Variable appDT = new Variable();
        appDT.setName("app");
        appDT.setAccess("private");
        appDT.setIsStatic(false);
        appDT.setType("ThreadExample");
        dtV.add(appDT);
        Variable now = new Variable();
        now.setAccess("private");
        now.setName("now");
        now.setIsStatic(false);
        now.setType("Date");
        dtV.add(now);
        dtC.setVariable(ctV);
        
        ArrayList<Method> dtM = new ArrayList();
        Method dtm = new Method();
        dtm.setIsAbstract(false);
        dtm.setAccess("public");
        dtm.setName("DateTask");
        dtm.setIsStatic(false);
        ArrayList dtmA = new ArrayList();
        arg = "ThreadExample";
        dtmA.add(arg);
        dtm.setArg(dtmA);
        dtM.add(dtm);
        
        Method callDT = new Method();
        callDT.setIsAbstract(false);
        callDT.setAccess("protected");
        callDT.setName("call");
        callDT.setReturnType("void");
        callDT.setIsStatic(false);
        dtM.add(callDT);

        dtC.setMethod(dtM);
        
        list.add(dtC); 
        
        JClass ph = new JClass();
        ph.setAbstract(false);
        ph.setInterface(false);
        ph.setName("PauseHandler");
        ph.setPackage("default package");
        ph.setAPI(false);
        ph.setParent("EventHandler");
        ArrayList<Variable> phV = new ArrayList();
        Variable appPH = new Variable();
        appPH.setName("app");
        appPH.setAccess("private");
        appPH.setIsStatic(false);
        appPH.setType("ThreadExample");
        phV.add(appPH);
        ph.setVariable(phV);
        
        ArrayList<Method> phM = new ArrayList();
        Method phC = new Method();
        phC.setIsAbstract(false);
        phC.setAccess("public");
        phC.setName("PauseHandler");
        phC.setIsStatic(false);
        ArrayList<String> phA = new ArrayList();
        arg = "ThreadExample";
        phA.add(arg);
        phC.setArg(phA);
        phM.add(phC);
        
        Method handleP = new Method();
        handleP.setIsAbstract(false);
        handleP.setAccess("public");
        handleP.setName("handle");
        handleP.setReturnType("void");
        handleP.setIsStatic(false);
        ArrayList<String> handlePA = new ArrayList();
        arg = "Event";
        handlePA.add(arg);
        handleP.setArg(handlePA);
        phM.add(handleP);
        
        ph.setMethod(phM);
        list.add(ph);
        
        JClass sh = new JClass();
        sh.setAbstract(false);
        sh.setInterface(false);
        sh.setName("StartHandler");
        sh.setPackage("default package");
        sh.setAPI(false);
        sh.setParent("EventHandler");
        ArrayList<Variable> shV = new ArrayList();
        Variable appSH = new Variable();
        appSH.setName("app");
        appSH.setAccess("private");
        appSH.setIsStatic(false);
        appSH.setType("ThreadExample");
        shV.add(appSH);
        sh.setVariable(shV);
        
        ArrayList<Method> shM = new ArrayList();
        Method shC = new Method();
        shC.setIsAbstract(false);
        shC.setAccess("public");
        shC.setName("PauseHandler");
        shC.setIsStatic(false);
        ArrayList<String> shA = new ArrayList();
        arg = "ThreadExample";
        shA.add(arg);
        shC.setArg(shA);
        shM.add(shC);
        
        Method handleS = new Method();
        handleS.setIsAbstract(false);
        handleS.setAccess("public");
        handleS.setName("handle");
        handleS.setReturnType("void");
        handleS.setIsStatic(false);
        ArrayList<String> handleSA = new ArrayList();
        arg = "Event";
        handleSA.add(arg);
        handleS.setArg(handleSA);
        shM.add(handleS);
        
        sh.setMethod(shM);
        list.add(sh);
        
        JClass task = new JClass();
        task.setAPI(true);
        task.setAbstract(false);
        task.setInterface(false);
        task.setName("Task");
        task.setPackage("javafx.concurrent");
        list.add(task);

        JClass date = new JClass();
        date.setAPI(true);
        date.setAbstract(false);
        date.setInterface(false);
        date.setName("Date");
        date.setPackage("java.util");
        list.add(date);
        
        JClass stage = new JClass();
        stage.setAPI(true);
        stage.setAbstract(false);
        stage.setInterface(false);
        stage.setName("Stage");
        stage.setPackage("javafx.stage");
        list.add(stage);
        
        JClass border = new JClass();
        border.setAPI(true);
        border.setAbstract(false);
        border.setInterface(false);
        border.setName("BorderPane");
        border.setPackage("javafx.scene.layout");
        list.add(border);
        
        JClass flow = new JClass();
        flow.setAPI(true);
        flow.setAbstract(false);
        flow.setInterface(false);
        flow.setName("FlowPane");
        flow.setPackage("javafx.scene.layout");
        list.add(flow);
        
        JClass button = new JClass();
        button.setAPI(true);
        button.setAbstract(false);
        button.setInterface(false);
        button.setName("Button");
        button.setPackage("javafx.scene.control");
        list.add(button);
        
        JClass scoll = new JClass();
        scoll.setAPI(true);
        scoll.setAbstract(false);
        scoll.setInterface(false);
        scoll.setName("ScrollPane");
        scoll.setPackage("javafx.scene.control");
        list.add(scoll);
        
        JClass text = new JClass();
        text.setAPI(true);
        text.setAbstract(false);
        text.setInterface(false);
        text.setName("TextArea");
        text.setPackage("javafx.scene.control");
        list.add(text);
        
        JClass thread = new JClass();
        thread.setAPI(true);
        thread.setAbstract(false);
        thread.setInterface(false);
        thread.setName("Thread");
        thread.setPackage("java.lang");
        list.add(thread);
        
        JClass eh = new JClass();
        eh.setAPI(true);
        eh.setAbstract(false);
        eh.setInterface(true);
        eh.setName("EventHandler");
        eh.setPackage("javafx.event");
        list.add(eh);
        
        
        DataManager dm = new DataManager();
        FileManager fm = new FileManager();
        dm.setJClassArray(list);
        
        String filePath = "work/DesignSaveTest.json";
        fm.saveData(dm, filePath);
        
        
    }
    
}
